#!/usr/bin/env bash

source .env
export PROJECT_ID=$PROJECT_ID

gcloud config set project $PROJECT_ID
gcloud services enable cloudfunctions.googleapis.com
gcloud services enable deploymentmanager.googleapis.com
gcloud services enable datastore.googleapis.com
gcloud services enable firestore.googleapis.com
gcloud services enable cloudscheduler.googleapis.com
gcloud pubsub topics create burja || true
gcloud app create --region=europe-west3 || true
gcloud beta scheduler jobs create pubsub burja --schedule="0 * * * *" --topic="burja" --time-zone="Europe/Prague" --message-body="ping" || true
