# burja-gcp

Simple Go application prepared for GCP Cloud Functions.

## Structure

This simple application is consists of two main parts.

### Parser

Works as a cron, fetches the data from [opendata.si](https://opendata.si/promet/burja/) every hour and stores it to the [Cloud Firestore](https://firebase.google.com/docs/firestore).

### API

Fetches the data from [Cloud Firestore](https://firebase.google.com/docs/firestore), process it and returns it to the client.

## Setup

First, be sure you have [gcloud SDK](https://cloud.google.com/sdk/install) installed, together with [beta components](https://cloud.google.com/sdk/gcloud/reference/components/install). Also, be sure to configure your account and project.

Download your GCP credentials and store them to `credentials.json`. Basically you need to do this:

_IAM -> Service Accounts -> Select service account and then Create Key (JSON) -> save to credentials.json_

But if you need additional help with this, check [Serverless readme](https://serverless.com/framework/docs/providers/google/guide/credentials#get-credentials--assign-roles).

```bash
cp .env.example .env
```

Add your `PROJECT_ID` to `.env` file.

```bash
npm install
```

Before first deploy, be sure to run setup, but do this only once.

```bash
make setup
```

## Deploy

```bash
make deploy
```
