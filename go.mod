module gitlab.com/kiwicom/go-sls-examples/burja-gcp

go 1.12

require (
	cloud.google.com/go v0.40.0
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.3.0 // indirect
)
