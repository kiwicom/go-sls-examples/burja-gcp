.PHONY: lint run setup deploy

lint:
	golint -set_exit_status

setup:
	./setup.sh

deploy:
	./deploy.sh
